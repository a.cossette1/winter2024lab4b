public class Owl {
	private String colour;
	private int age;
	private int flightSpeed;
	
	//Set Methods
	public void setFlightSpeed(int flightSpeed) {
		this.flightSpeed = flightSpeed;
	}
	
	//Get Methods
	public String getColour() {
		return this.colour;
	}
	public int getAge() {
		return this.age;
	}
	public int getFlightSpeed() {
		return this.flightSpeed;
	}
	
	//Constructor
	public Owl(String colour, int age, int flightSpeed) {
		this.colour = colour;
		this.age = age;
		this.flightSpeed = flightSpeed;
	}
	
	public void sleep() {
		System.out.println("The " + this.colour + ", " + this.age + " year old owl is sleeping");
	}
	
	public void hunt() {
		System.out.println("The owl will hunt its prey at a speed of " + this.flightSpeed + " km/h");
	}
}