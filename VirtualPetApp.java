import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Owl[] parliament = new Owl[1];
		
		//Loop that assigns values to each owl in the parliament
		for(int i = 0; i < parliament.length; i++) {
	
			System.out.println("What colour is this owl? ");
			String owlColour = reader.nextLine();
			//parliament[i].setColour(owlColour);
			
			System.out.println("How old is this owl? ");
			int owlAge = Integer.parseInt(reader.nextLine());
			//parliament[i].setAge(owlAge);
			
			System.out.println("What speed does this owl fly at? ");
			int owlFlightSpeed = Integer.parseInt(reader.nextLine());
			//parliament[i].setFlightSpeed(owlFlightSpeed);
			
			parliament[i] = new Owl(owlColour, owlAge, owlFlightSpeed);
		}
		System.out.println(parliament[parliament.length - 1].getColour());
		System.out.println(parliament[parliament.length - 1].getAge());
		System.out.println(parliament[parliament.length - 1].getFlightSpeed());
		
		System.out.println("Update the flight speed of your owl!");
		parliament[parliament.length - 1].setFlightSpeed(Integer.parseInt(reader.nextLine()));
		
		System.out.println(parliament[parliament.length - 1].getColour());
		System.out.println(parliament[parliament.length - 1].getAge());
		System.out.println(parliament[parliament.length - 1].getFlightSpeed());
		
	}
}

